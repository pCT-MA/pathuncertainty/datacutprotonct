#include <TBranch.h>
#include <TDirectory.h>
#include <TFile.h>
#include <TTree.h>

#include "DataCutProtonCT.h"
#include "core/utils/file.h"
#include "objects/Multiplet.hpp"

namespace corryvreckan {
    DataCutProtonCT::DataCutProtonCT(Configuration& config, std::vector<std::shared_ptr<Detector>> detectors)
        : Module(config, detectors), mDistributionFileName(""), mEnergyLossIntervall{0, 0}, mScatteringIntervall{0, 0},
          mDistributionTree(nullptr), mEnergyLoss(0), mKinkAngleX(0), mKinkAngleY(0) {
        mLastFrontDetector = config.get<std::string>("last_front_detector");
        mFirstBackDetector = config.get<std::string>("first_back_detector");

        auto fileName = config.get<std::string>("distribution_file", "");
        try {
            mDistributionFileName = corryvreckan::get_canonical_path(fileName);
            mApplyCuts = true;
        } catch(std::invalid_argument const&) {
            mApplyCuts = false;
        }

        bool foundFront = false;
        bool foundBack = false;
        for(const auto& detector : detectors) {
            if(detector->getName() == mLastFrontDetector) {
                foundFront = true;
            }
            if(detector->getName() == mFirstBackDetector) {
                foundBack = true;
            }
        }

        if(!foundFront || !foundBack) {
            ModuleError("Missing specification for lastFrontDetector or firstBackDetector");
        }
    }

    void DataCutProtonCT::initialize() {
        if(mApplyCuts) {
            LoadDistributions();
        } else {
            InitializeDistributionOutput();
        }
    }

    StatusCode DataCutProtonCT::run(const std::shared_ptr<Clipboard>& clipboard) {
        if(mApplyCuts) {
            return ApplyCuts(clipboard);
        }
        return FindDistributions(clipboard);
    }

    void DataCutProtonCT::finalize(const std::shared_ptr<ReadonlyClipboard>&) {
        if(mDistributionTree != nullptr) {
            getROOTDirectory()->WriteTObject(mDistributionTree);
            delete mDistributionTree;
            mDistributionTree = nullptr;
        }
    }

    void DataCutProtonCT::LoadDistributions() {
        typedef std::array<double, 2> MeanStd;

        auto addToDistribution = [](MeanStd& distribution, double value) {
            distribution[0] += value;
            distribution[1] += value * value;
        };
        auto finishDistribution = [](MeanStd& distribution, Long64_t n) {
            distribution[0] /= static_cast<double>(n);
            distribution[1] =
                std::sqrt(distribution.at(1) / static_cast<double>(n) - distribution.at(0) * distribution.at(0));
        };
        auto interval = [](MeanStd const& distribution, double numberOfStds = 3) {
            return Interval{distribution.at(0) - numberOfStds * distribution.at(1),
                            distribution.at(0) + numberOfStds * distribution.at(1)};
        };

        TFile distributionFile(mDistributionFileName.c_str());
        auto directory = distributionFile.Get<TDirectory>(getUniqueName().c_str());
        auto distributionTree = directory->Get<TTree>(gDistributionTreeName);
        double energyLoss;
        double kinkAngleX;
        double kinkAngleY;
        distributionTree->SetBranchAddress(gEnergyLossBranchName, &energyLoss, nullptr);
        distributionTree->SetBranchAddress(gScatteringXBranchName, &kinkAngleX, nullptr);
        distributionTree->SetBranchAddress(gScatteringYBranchName, &kinkAngleY, nullptr);
        const auto n = distributionTree->GetEntries();

        MeanStd energyLossDistribution{0, 0};
        MeanStd scatteringDistribution{0, 0};
        for(Long64_t i = 0; i < n; i++) {
            distributionTree->GetEntry(i);
            addToDistribution(energyLossDistribution, energyLoss);
            addToDistribution(scatteringDistribution, kinkAngleX);
            addToDistribution(scatteringDistribution, kinkAngleY);
        }
        finishDistribution(energyLossDistribution, n);
        finishDistribution(scatteringDistribution, 2 * n);

        mEnergyLossIntervall = interval(energyLossDistribution);
        mScatteringIntervall = interval(scatteringDistribution);
    }

    void DataCutProtonCT::InitializeDistributionOutput() {
        mDistributionTree = new TTree(gDistributionTreeName, "Data cut distributions");
        mDistributionTree->Branch(gEnergyLossBranchName, &mEnergyLoss);
        mDistributionTree->Branch(gScatteringXBranchName, &mKinkAngleX);
        mDistributionTree->Branch(gScatteringYBranchName, &mKinkAngleY);
    }

    StatusCode DataCutProtonCT::FindDistributions(const std::shared_ptr<Clipboard>& clipboard) {
        // I would have liked to getData<Multiplet>, but CV returns {} in this case
        // because it falls back to the base class Track
        auto multipletVector = clipboard->getData<Track>();
        auto frontCluster = clipboard->getData<Pixel>(mLastFrontDetector);
        auto backCluster = clipboard->getData<Pixel>(mFirstBackDetector);

        if(multipletVector.size() == 1 && frontCluster.size() == 1 && backCluster.size() == 1) {
            mEnergyLoss = frontCluster.front()->raw() - backCluster.front()->raw();
            const auto kink = multipletVector.front()->getDirection(mFirstBackDetector) -
                              multipletVector.front()->getDirection(mLastFrontDetector);
            mKinkAngleX = kink.X();
            mKinkAngleY = kink.Y();
            mDistributionTree->Fill();

            return StatusCode::Success;
        }
        return StatusCode::NoData;
    }

    StatusCode DataCutProtonCT::ApplyCuts(const std::shared_ptr<Clipboard>& clipboard) {
        auto inInterval = [](Interval const& interval, double value) {
            return interval.first <= value && value <= interval.second;
        };
        auto result = StatusCode::NoData;
        auto multipletVector = clipboard->getData<Track>();
        auto frontCluster = clipboard->getData<Pixel>(mLastFrontDetector);
        auto backCluster = clipboard->getData<Pixel>(mFirstBackDetector);

        if(multipletVector.size() == 1 && frontCluster.size() == 1 && backCluster.size() == 1) {
            const auto energyLoss = frontCluster.front()->raw() - backCluster.front()->raw();
            const auto kink = multipletVector.front()->getDirection(mFirstBackDetector) -
                              multipletVector.front()->getDirection(mLastFrontDetector);
            result = inInterval(mEnergyLossIntervall, energyLoss) && inInterval(mScatteringIntervall, kink.X()) &&
                             inInterval(mScatteringIntervall, kink.Y())
                         ? StatusCode::Success
                         : StatusCode::DeadTime; // skips modules after this one
        }
        return result;
    }
} // namespace corryvreckan
