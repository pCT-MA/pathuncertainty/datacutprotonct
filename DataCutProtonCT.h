#pragma once

#include "core/module/Module.hpp"
#include "objects/Cluster.hpp"
#include "objects/Pixel.hpp"
#include "objects/Track.hpp"

class TTree;

namespace corryvreckan {
    class DataCutProtonCT : public Module {
    public:
        DataCutProtonCT(Configuration& config, std::vector<std::shared_ptr<Detector>> detectors);
        ~DataCutProtonCT() {}

        void initialize() override;
        StatusCode run(const std::shared_ptr<Clipboard>& clipboard) override;
        void finalize(const std::shared_ptr<ReadonlyClipboard>& clipboard) override;

    private:
        static constexpr const char* gDistributionTreeName = "DataCutDistributions";
        static constexpr const char* gEnergyLossBranchName = "EnergyLoss";
        static constexpr const char* gScatteringXBranchName = "ScatteringX";
        static constexpr const char* gScatteringYBranchName = "ScatteringY";

        // config
        bool mApplyCuts;
        std::string mDistributionFileName;
        std::string mLastFrontDetector;
        std::string mFirstBackDetector;

        // cutoffs
        typedef std::pair<double, double> Interval;
        Interval mEnergyLossIntervall;
        Interval mScatteringIntervall;

        // temporaries for root
        TTree* mDistributionTree;
        double mEnergyLoss;
        double mKinkAngleX;
        double mKinkAngleY;

        void LoadDistributions();
        void InitializeDistributionOutput();

        StatusCode FindDistributions(const std::shared_ptr<Clipboard>& clipboard);
        StatusCode ApplyCuts(const std::shared_ptr<Clipboard>& clipboard);
    };
} // namespace corryvreckan
